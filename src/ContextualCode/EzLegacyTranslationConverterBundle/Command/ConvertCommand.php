<?php

namespace ContextualCode\EzLegacyTranslationConverterBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ConvertCommand extends ContainerAwareCommand
{

    protected $output = null;

    /**
     * Configures the command
     */
    protected function configure()
    {
        $this->setName('cc:ezlegacy_translation_convert');
        $this->setDescription('');
        $this->setDefinition(
            array(
                new InputOption('legacy_trans_dir', null, InputOption::VALUE_REQUIRED, 'Path to legacy extension translations dir to load translations from'),
                new InputOption('dest_dir', null, InputOption::VALUE_REQUIRED, 'Directory to put finished translations'),
                new InputOption('to_json', null, InputOption::VALUE_REQUIRED, 'Output as JSON'),
            )
        );
    }

    /**
     * Executes the command
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->output = $output;

        $this->output->writeln("<info>Starting...</info>");

        $legacyTransDir = $input->getOption('legacy_trans_dir');
        if (empty($legacyTransDir)) {
            $this->output->writeln("<error>Please supply a --legacy_trans_dir option.</error>");
            return;
        }
        $destDir = $input->getOption('dest_dir');
        if (empty($destDir)) {
            $this->output->writeln("<error>Please supply a --dest_dir option.</error>");
            return;
        }
        $toJson = $input->getOption('to_json');

        $converterService = $this->getContainer()->get("cc.ezlegacy_translation_converter");
        $converterService->convert($legacyTransDir, $destDir, $toJson, $output);

        $this->output->writeln("<info>Done!</info>");
    }

}
