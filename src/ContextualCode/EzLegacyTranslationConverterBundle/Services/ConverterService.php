<?php

namespace ContextualCode\EzLegacyTranslationConverterBundle\Services;

class ConverterService
{

    protected $legacyTransDir = null;
    protected $destDir = null;

    public function __construct()
    {
    }

    public function convert($legacyTransDir, $destDir, $toJson = false, $output = false)
    {
        $this->destDir = $destDir;
        if (!is_dir($this->destDir)) {
            mkdir($this->destDir, 0755, true);
        }
        $this->legacyTransDir = $legacyTransDir;
        $dirArray = $this->dirToArray($this->legacyTransDir);

        foreach ($dirArray as $lang => $transFileArray) {
            foreach ($transFileArray as $transFile) {
                if (strpos($transFile, '.') === 0) {
                    continue;
                }
                $this->handleFile($lang, $transFile, $toJson, $output);
            }
        }
    }

    protected function outputMessage($message, $output = false)
    {
        if ($output) {
            $output->writeln($message);
        }
        else {
            print_r($output);
        }
    }

    protected function handleFile($lang, $transFile, $toJson = false, $output = false)
    {
        $filename = $this->legacyTransDir . '/' . $lang . '/' . $transFile;
        if (!file_exists($filename)) {
            $this->outputMessage("File " . $filename . " does not exist", $output);
            return;
        }

        $transArray = array();

        $xml = simplexml_load_file($filename);
        $i = 0;
        foreach ($xml->context as $context) {
            $i++;
            if (!isset($context->name)) {
                $this->outputMessage("'name' not set in $filename context $i \r\n", $output);
                continue;
            }
            if (!isset($context->message)) {
                $this->outputMessage("'message' not set in $filename context $i \r\n", $output);
                continue;
            }

            $domain = $this->convertNameToDomain($context->name);

            if (!isset($transArray[$domain])) {
                $transArray[$domain] = array();
            }

            $j = 0;
            foreach ($context->message as $message) {
                $j++;
                if (!isset($message->source)) {
                    $this->outputMessage("'source' not set in $filename message $j \r\n", $output);
                    continue;
                }
                if (!isset($message->translation)) {
                    $this->outputMessage("'translation' not set in $filename message $j \r\n", $output);
                    continue;
                }

                $unfinished = false;
                foreach ($message->translation->attributes() as $key => $val) {
                    if ($key == 'type' && $val == 'unfinished') {
                        $unfinished = true;
                        break;
                    }
                }
                if ($unfinished) {
                    continue;
                }

                $source = strval($message->source);
                $dest = strval($message->translation);

                if (empty(trim($dest))) {
                    continue;
                }

                $transArray[$domain][$source] = $dest;
            }
        }

        foreach ($transArray as $domain => $translations) {
            if (count($translations) === 0) {
                continue;
            }
            $destFilepath = $this->getDestFilepath($lang, $domain, $toJson);
            if ($toJson) {
                $text = json_encode($translations, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
            }
            else {
                $text = \yaml_emit($translations, YAML_UTF8_ENCODING);
            }
            file_put_contents($destFilepath, $text);
            $this->outputMessage("Wrote to $destFilepath \r\n", $output);
        }

    }

    protected function getDestFilepath($lang, $domain, $toJson = false) {
        return $this->destDir . '/' . $domain . '.' . $lang . ($toJson ? '.json' : '.yaml');
    }

    protected function convertNameToDomain($name)
    {
        $name = str_replace('design/', '', $name);
        $name = str_replace('/', '_', $name);
        return $name;
    }

    protected function dirToArray($dir) {
        $result = array();
        $cdir = scandir($dir);
        foreach ($cdir as $key => $value) {
            if (!in_array($value,array('.','..'))) {
                if (is_dir($dir . DIRECTORY_SEPARATOR . $value)) {
                    $result[$value] = $this->dirToArray($dir . DIRECTORY_SEPARATOR . $value);
                }
                else {
                    $result[] = $value;
                }
            }
        }
        return $result;
    }

}
