# EzLegacyTranslationConverterBundle

This bundle provides an Symfony Service and Command to convert eZ Legacy translation files to .xlf files for Symfony (eZ Platform) translations.

## Installation
Add this repo to your `composer.json` file configuration:
```
composer config repositories.ezlegacytranslationbundle vcs git@gitlab.com:contextualcode/EzLegacyTranslationConverterBundle.git
composer require contextualcode/ezlegacy-translation-converter-bundle
```

Enable the bundle in `app/AppKernel.php` (`ezpublish/EzPublishKernel.php`) by adding this line in the `registerBundles` method:

```php
    public function registerBundles()
    {
        $bundles = array(
            ...
            new ContextualCode\EzLegacyTranslationConverterBundle\ContextualCodeEzLegacyTranslationConverterBundle(),
            ...
        );
```
## Example Usage

This assumes you have a `my_legacy_project` directory as a sibling to the project you've installed this bundle on, that has a legacy extension called `my_extension` that has translations files in it. This will output JSON translation files into your project's `translations` directory.

```bash
php bin/console cc:ezlegacy_translation_convert --legacy_trans_dir=../my_legacy_project/ezpublish_legacy/extension/my_extension/translations --dest_dir=translations --to_json=true
```